# Import configuration values from JSON file
$config = Get-Content -Path '.\config.json' -Raw | ConvertFrom-Json -AsHashtable

# Create an EC2 instance
$ec2Parameters = $config.EC2Parameters
$instance = New-EC2Instance @ec2Parameters

# Add a pause to make sure AWS has time to create the instance and make it available
Start-Sleep -s 10

# Get the InstanceId of the recently created EC2 instance
$instanceId = $instance.Instances.InstanceId

# Create tags
$tags = New-Object Amazon.EC2.Model.Tag
$tags.Key = "Name"
$tags.Value = $config.Project + ' Server'

# Add the tags to the instance
New-EC2Tag -Resources $instanceId -Tags $tags
